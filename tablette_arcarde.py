"""
Cubito
"""

import arcade
# import cubito

# Screen title and size
SCREEN_TITLE = "Cubito"
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

# Margin between mat & screen side
VERTICAL_MARGIN_PERCENT = 0.10
HORIZONTAL_MARGIN_PERCENT = 0.10

# Token size
TOKEN_SCALE = 0.5
TOKEN_HEIGHT = 75 * TOKEN_SCALE
TOKEN_WIDTH = 75 * TOKEN_SCALE

# Space between tokens
X_SPACING_TOKEN = TOKEN_WIDTH + TOKEN_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_TOKEN = TOKEN_HEIGHT + TOKEN_HEIGHT * VERTICAL_MARGIN_PERCENT

# List of token types
TOKEN_TYPES = ["FORDWARD", "RIGHT", "LEFT", "PAUSE", "FUNCTION"]

# Mat size
MAT_PERCENT_OVERSIZE = 1.25
MAT_HEIGHT = int(TOKEN_HEIGHT * MAT_PERCENT_OVERSIZE)
MAT_WIDTH = int(TOKEN_WIDTH * MAT_PERCENT_OVERSIZE)

# Number of column & row mat
MAT_COLUMN = 4
MAT_ROW = 4
MAT_FUNCTION_ROW = 2

# Space between mats
X_SPACING_MAT = MAT_WIDTH + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_MAT = MAT_HEIGHT + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Top for mats
TOP_Y = SCREEN_HEIGHT - MAT_HEIGHT - MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Bottom for tokens
BOTTOM_Y = MAT_HEIGHT / 2 + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Start from left side
START_X = MAT_WIDTH / 2 + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT


class Cubito(arcade.Window):
    """Main application class"""

    def __init__(self):
        # Init parent class
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        # Set background color
        arcade.set_background_color(arcade.color.AMAZON)

        # List of tokens
        self.token_list = None

        # Hold token
        self.held_token = None

        # Origin pos for hold token
        self.held_token_original_position = None

        # List of mats
        self.mat_list = None

        # List of mats function
        self.mat_function_list = None

    def setup(self):
        """Set up the game"""
        ...

    def on_draw(self):
        """Render the screen"""
        # Clear the screen
        arcade.start_render()

    def on_mouse_press(self, x, y, button, key_modifiers):
        """Called when the user presses a mouse button"""
        ...

    def on_mouse_release(self, x, y, button, modifiers):
        """Called when the user presses a mouse button"""
        ...

    def on_mouse_motion(self, x, y, dx, dy):
        """Called when the user moves the mouse"""
        ...

    def on_key_press(self, symbol, modifiers):
        """Called when the user presses key"""
        ...

    def cubito(self, function=False):
        """Move cubito !"""
        ...

def main():
    """Main method"""
    window = Cubito()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
