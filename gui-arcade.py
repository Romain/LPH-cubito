"""
Cubito
ressources : https://api.arcade.academy/en/latest/resources.html
"""

import arcade
import cubito
import os.path

SCREEN_MULTILPLIER = 2
SCREEN_MULTILPLIER /= 4

# Screen title and size
SCREEN_TITLE = "Cubito"
SCREEN_WIDTH = int(2000* SCREEN_MULTILPLIER)
SCREEN_HEIGHT = int(2000* SCREEN_MULTILPLIER)

#Token row
TOKEN_ROW = 8

# Token size
TOKEN_HEIGHT = int(160 * SCREEN_MULTILPLIER)
TOKEN_WIDTH = int(160 * SCREEN_MULTILPLIER)

# Token scale
HELD_TOKEN_SCALE_MULTILPLIER = 1.4

# Space between tokens
X_SPACING_TOKEN = int(45*SCREEN_MULTILPLIER + TOKEN_WIDTH)
Y_SPACING_TOKEN = int(45*SCREEN_MULTILPLIER + TOKEN_HEIGHT)

# Token start
X_TOKEN_START = int(108*SCREEN_MULTILPLIER + TOKEN_WIDTH / 2)
Y_TOKEN_START = int(1618*SCREEN_MULTILPLIER + TOKEN_HEIGHT / 2)

# List of token types
TOKEN_TYPES = ["up", "left", "right", "function"]

#Mat start
X_MAT_START = int(1072 * SCREEN_MULTILPLIER)
Y_MAT_START = int(1648 * SCREEN_MULTILPLIER)
X_MAT_FUNCTION_START = X_MAT_START
Y_MAT_FUNCTION_START =  int(510  * SCREEN_MULTILPLIER)

# Mat size
MAT_HEIGHT = int(200 * SCREEN_MULTILPLIER)
MAT_WIDTH = int(200 * SCREEN_MULTILPLIER)

# Number of column & row mat
MAT_COLUMN = 4
MAT_ROW = 4
MAT_FUNCTION_ROW = 2

# Space between mats
X_SPACING_MAT = int(MAT_WIDTH + 30 * SCREEN_MULTILPLIER)
Y_SPACING_MAT = int(MAT_WIDTH + 30 * SCREEN_MULTILPLIER)

# Image
X_START_MAIN_MAT_IMAGE = 1415 * SCREEN_MULTILPLIER
Y_START_MAIN_MAT_IMAGE = 1304 * SCREEN_MULTILPLIER
X_START_FUNCTION_MAT_IMAGE = X_START_MAIN_MAT_IMAGE
Y_START_FUNCTION_MAT_IMAGE = 393 * SCREEN_MULTILPLIER

# Couleur
BACKGROUND_COLOR = (133, 100, 100)


CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

class Start_mat(arcade.SpriteSolidColor):
    def __init__(self, height, width, color=arcade.color.AMBER):
        super().__init__(width, height, color)

class Mat(arcade.SpriteSolidColor):
    def __init__(self, height, width, color=arcade.color.AMBER):
        super().__init__(width, height, color)

class Mat_function(arcade.SpriteSolidColor):
    def __init__(self, height, width, color=arcade.color.AMBER):
        super().__init__(width, height, color)

class Token_sprite(arcade.Sprite):
    """ Token sprite """

    def __init__(self, token_type, scale=1):
        # Attributes for token type
        self.token_type = token_type
        scale *= SCREEN_MULTILPLIER

        self.image_file_name = f"{CURRENT_PATH}/Img/token/{self.token_type}.png"

        # Call the parent
        super().__init__(self.image_file_name, scale, hit_box_algorithm="None")

class Image(arcade.Sprite):
    def __init__(self, file_name, scale=1):
        self.image_file_name = f"{CURRENT_PATH}/Img/{file_name}"
        scale *= SCREEN_MULTILPLIER
        # Call the parent
        super().__init__(self.image_file_name, scale, hit_box_algorithm="None")



class Cubito(arcade.Window):
    """Main application class"""

    def __init__(self):
        # Init parent class
        super().__init__(int(SCREEN_WIDTH), int(SCREEN_HEIGHT), SCREEN_TITLE)
        # Set background color
        arcade.set_background_color(BACKGROUND_COLOR)

        # List of tokens
        self.token_list = None

        # Hold token
        self.held_token = None

        # Origin pos for hold token
        self.held_token_original_position = None

        #List of start mats
        self.start_mat_list = None

        # List of mats
        self.mat_list = None

        # List of mats function
        self.mat_function_list = None

        # List of mats
        self.image_list = None

    def setup(self):
        """Set up the game"""

        # Token we are dragging with the mouse
        self.held_token = None

        # Original location of token we are dragging with the mouse in case
        # they have to go back.
        self.held_token_original_position = None

        self.token_list = arcade.SpriteList()
        self.mat_list = arcade.SpriteList()
        self.start_mat_list = arcade.SpriteList()
        self.mat_function_list = arcade.SpriteList()
        self.image_list = arcade.SpriteList()

        for y in range(TOKEN_ROW):
            x = X_TOKEN_START
            for token_type in TOKEN_TYPES:
                #placer les tokens
                token = Token_sprite(token_type)
                token.position = x, Y_TOKEN_START - Y_SPACING_TOKEN * y
                x += X_SPACING_TOKEN
                self.token_list.append(token)
                token.token_type

                #placer des cases sous les tokens
                start_mat = Start_mat(MAT_HEIGHT, MAT_WIDTH, color=arcade.color.BEIGE)
                start_mat.position = x - X_SPACING_TOKEN, Y_TOKEN_START - Y_SPACING_TOKEN * y
                self.start_mat_list.append(start_mat)

        # Placer les cases principales
        for y in range(MAT_ROW):
            for x in range(MAT_COLUMN):
                mat = Mat(MAT_HEIGHT, MAT_WIDTH)
                mat.position = X_MAT_START + X_SPACING_MAT * x, Y_MAT_START - Y_SPACING_MAT * y
                self.mat_list.append(mat)

        # Placer les cases fonctions
        for y in range(MAT_FUNCTION_ROW):
            for x in range(MAT_COLUMN):
                mat = Mat_function(MAT_HEIGHT, MAT_WIDTH, arcade.color.BABY_BLUE)
                mat.position = X_MAT_FUNCTION_START + X_SPACING_MAT * x, Y_MAT_FUNCTION_START - Y_SPACING_MAT * y
                self.mat_function_list.append(mat)

        # Placer les images
        image = Image(file_name="case/Principal.png", scale=1)
        image.position = X_START_MAIN_MAT_IMAGE, Y_START_MAIN_MAT_IMAGE
        self.image_list.append(image)

        image = Image(file_name="case/Fonction.png", scale=1)
        image.position = X_START_FUNCTION_MAT_IMAGE, Y_START_FUNCTION_MAT_IMAGE
        self.image_list.append(image)

    def on_draw(self):
        """Render the screen"""
        # Clear the screen
        self.clear()

        #Draw the images
        self.image_list.draw()

        #Draw the mat
        # self.mat_list.draw()
        # self.start_mat_list.draw()
        # self.mat_function_list.draw()

        # Draw the token
        self.token_list.draw()


    def pull_to_top(self, token: arcade.Sprite):
        """ Pull token to top of rendering order (last to render, looks on-top) """

        # Remove, and append to the end
        self.token_list.remove(token)
        self.token_list.append(token)

    def on_mouse_press(self, x, y, button, key_modifiers):
        """Called when the user presses a mouse button"""

        # Get list of tokens we've clicked on
        tokens = arcade.get_sprites_at_point((x, y), self.token_list)


        # Have we clicked on a token?
        if len(tokens) > 0:

            # Might be a stack of tokens, get the top one
            primary_token = tokens[-1]

            # All other cases, grab the token we are clicking on
            self.held_token = primary_token
            # Save the position
            self.held_token_original_position = self.held_token.position

            # Expand the size of token
            self.held_token.scale *= HELD_TOKEN_SCALE_MULTILPLIER

            # Put on top in drawing order
            self.pull_to_top(self.held_token)


    def on_mouse_release(self, x, y, button, modifiers):
        """Called when the user presses a mouse button"""
        def collision(reset_position, list_mat):
            # Find the closest mat, in case we are in contact with more than one
            mat, distance = arcade.get_closest_sprite(self.held_token, list_mat)

            # See if we are in contact with the closest mat
            if arcade.check_for_collision(self.held_token, mat):
                # Reduce the size of token
                self.held_token.scale /= HELD_TOKEN_SCALE_MULTILPLIER

                # Center the token
                self.held_token.position = mat.center_x, mat.center_y

                # Success, don't reset position of tokens
                reset_position = False
            
            return reset_position

        
        # If we don't have any tokens, who cares
        if self.held_token == None:
            return

        reset_position = True
        reset_position = collision(reset_position, self.mat_list)
        reset_position = collision(reset_position, self.start_mat_list)
        reset_position = collision(reset_position, self.mat_function_list)

        if reset_position:
            # Where-ever we were dropped, it wasn't valid. Reset the each token's position
            # to its original spot.
            self.held_token.position = self.held_token_original_position
            
            # Reduce the size of token
            self.held_token.scale /= HELD_TOKEN_SCALE_MULTILPLIER


        # We are no longer holding tokens
        self.held_token = None


    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        """Called when the user moves the mouse"""

        # If we are holding token, move it with the mouse
        if self.held_token != None:
            self.held_token.center_x += dx
            self.held_token.center_y += dy


    def on_key_press(self, symbol, modifiers):
        """Called when the user presses key"""
        if symbol == arcade.key.R:
            self.setup()
            cubito.reset()
            print("Restart !")

        if symbol == arcade.key.Q:
            arcade.exit()

        if symbol == arcade.key.S:
            self.cubito()


    def cubito(self, function=False):
        """Move cubito !"""

        if function:
            list = self.mat_function_list
        else:
            list = self.mat_list
        
        for mat in list:
            token, distance = arcade.get_closest_sprite(mat, self.token_list)
            if arcade.check_for_collision(token, mat):
                token_type = str(token.token_type)
                if token_type == "up":
                    cubito.fordward()
                if token_type == "left":
                    cubito.left()
                if token_type == "right":
                    cubito.right()
                if token_type == "function":
                    if function:
                        return
                    else:
                        self.cubito(function=True)



def main():
    """Main method"""
    window = Cubito()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()