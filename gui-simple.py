import PySimpleGUI as sg
import cubito
#python arcad
 
sg.theme('Dark')   # Add a touch of color
# All the stuff inside your window.
choice = ["FORWORD", "RIGHT", "LEFT", "FONCTION"]

#fonctions = [sg.Combo(choice, key="fonction1"), sg.Combo(choice, key="fonction2"), sg.Combo(choice, key="fonction3"), sg.Combo(choice, key="fonction4")]
fonctions = [sg.Combo(choice, key=f"fonction{i}") for i in range(4)]

#combo_box_line = [sg.Combo(choice) for _ in range(4)]

layout = [[sg.Button("run")],
          [sg.Combo(choice), sg.Combo(choice), sg.Combo(choice), sg.Combo(choice)],
          [sg.Combo(choice), sg.Combo(choice), sg.Combo(choice), sg.Combo(choice)],
          [sg.Combo(choice), sg.Combo(choice), sg.Combo(choice), sg.Combo(choice)],
          [sg.Combo(choice), sg.Combo(choice), sg.Combo(choice), sg.Combo(choice)],
          [sg.Text("Fonction")],
          fonctions
          ]


# Create the Window
window = sg.Window('Window Title', layout)
# Event Loop to process "events" and get the "values" of the inputs

#layout = []

def mouvement(value):
    if value == "FORWORD":
        cubito.fordward()
    if value == "LEFT":
        cubito.left()
    if value == "RIGHT":
        cubito.right()

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED:
        break
    if event == "run":
        for key, value in values.items():
            if "fonction" not in str(key):
                mouvement(value)
            if value == "FONCTION":
                for key_f, value_f in values.items():
                    if "fonction" in str(key_f):
                        mouvement(value_f)

window.close()