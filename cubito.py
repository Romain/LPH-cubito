import turtle
import os.path

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))
# Cell size
distance = 90

def start():
    # Set background - ocean
    turtle.bgpic(f"{CURRENT_PATH}/background.png")

    # Set turtle cusor
    turtle.pensize(10)
    turtle.pencolor("gray")
    turtle.color("red", "lightgreen")
    turtle.shape("turtle")
    turtle.shapesize(2)

    # Goto to first cell
    turtle.penup()
    turtle.goto(-225,225)
    #turtle.pendown()


def fordward():
    """
    Forward fonction
    """
    print(f"x : {turtle.xcor()}, y : {turtle.ycor()}")
    turtle.forward(distance)
    
    if turtle.xcor() > 226 or turtle.xcor() < -226:
        turtle.backward(distance)
    if turtle.ycor() > 226 or turtle.ycor() < -226:
        turtle.backward(distance)

def left():
    """
    Turn left fonction
    """
    turtle.left(90)

def right():
    """
    Turn right fonction
    """
    turtle.right(90)

def reset():
    turtle.clearscreen()
    start()


start()